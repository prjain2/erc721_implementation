// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/utils/Counters.sol";

interface IERC165 {
    function supportsInterface(bytes4 interfaceID) external view returns (bool);
}

/// @title ERC-721 Non-Fungible Token Standard
interface IERC721 {
    function balanceOf(address _owner) external view returns (uint256);

    function ownerOf(uint256 _tokenId) external view returns (address);

    function safeTransferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    ) external payable;

    function transferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    ) external payable;

    function approve(address _to, uint256 _tokenId) external payable;

    function getApproved(uint256 _tokenId) external view returns (address);

    function setApprovalForAll(address _operator, bool _approved) external;

    function isApprovedForAll(
        address _owner,
        address _operator
    ) external returns (bool);

    function safeTransferFrom(
        address _from,
        address _to,
        uint256 _tokenId,
        bytes calldata _data
    ) external payable;

    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _tokenId
    );

    event Approval(
        address indexed _owner,
        address indexed _approved,
        uint256 _tokenId
    );

    event ApprovalForAll(address _owner, address _operator, bool _approved);
}

interface ERC721TokenReceiver {
    function onERC721Received(
        address _operator,
        address _from,
        uint256 _tokenId,
        bytes calldata _data
    ) external returns (bytes4);
}

contract erc721Token is IERC721, IERC165 {
    string private tokenName;
    string private symbol;
    uint256 private tokenId;
    address private contractAddress;
    address private mintOwner;

    mapping(address => uint256) private _ownedTokenCount;
    mapping(uint256 => address) private _ownerAddress;
    mapping(uint256 => address) private _approves;
    mapping(address => mapping(address => bool)) private _approveAllToken;

    using Counters for Counters.Counter;
    Counters.Counter private _tokenIdTracker;

    constructor(
        string memory _tokenName,
        string memory _symbol,
        address _contractAddress
    ) {
        tokenName = _tokenName;
        symbol = _symbol;
        mintOwner = msg.sender;
        contractAddress = _contractAddress;
        _mint(msg.sender, _tokenIdTracker.current());
        _tokenIdTracker.increment();
    }

    function supportsInterface(
        bytes4 interfaceID
    ) public pure override returns (bool) {
        return interfaceID == 0x01ffc9a7 || interfaceID == 0x80ac58cd;
    }

    modifier onlyOwner() {
        require(msg.sender == mintOwner, "not owner");
        _;
    }

    /// @notice _mint mint the new NFT with _tokenId to _to address
    /// @dev only the owner can minted the new NFT else throw 'not owner'
    /// @param _to is the address where NFT minted by owner
    function mint(address _to) public virtual onlyOwner {
        _mint(_to, _tokenIdTracker.current());
        _tokenIdTracker.increment();
    }

    /// @notice _mint mint the new NFT with _tokenId to _to address
    /// @dev throw if _to is zero address, _tokenId does already exist
    /// @param _to address where mint a new NFT
    /// @param _tokenId is the new minted ID
    function _mint(address _to, uint256 _tokenId) internal virtual {
        address ownerOfToken = _ownerAddress[_tokenId];
        require(_to != address(0));
        require(ownerOfToken == address(0), "token already minted");
        _ownerAddress[_tokenId] = msg.sender;
        _ownedTokenCount[msg.sender]++;
    }

    /// @notice count all NFT assigned to an owner
    /// @dev NFT assigned to zero address are considered to be invalid
    /// @param _owner an address whom to check the balance
    /// @return the number of NFTs owned by _owner
    function balanceOf(
        address _owner
    ) public view virtual override returns (uint256) {
        require(_owner != address(0), "Invalid Address....");
        return _ownedTokenCount[_owner];
    }

    /// @notice funcd the owner of NFT
    /// @dev NFT assigned to zero address are considered to be invalid
    /// @param _tokenId the identifier of NFT
    /// return the address of the owner of the NFT
    function ownerOf(
        uint256 _tokenId
    ) public view virtual override returns (address) {
        address ownerOfToken = _ownerAddress[_tokenId];
        require(
            ownerOfToken != address(0),
            "Invalid Address....owner not exist with this tokenId"
        );
        return ownerOfToken;
    }

    /// @notice change the approved address for an NFT
    /// @dev NFT assigned to zero address are considered to be invalid and throw if the owner is _to address , unless msg.sender is the current NFT owner, or an authorized
    /// operator of the current owner.
    /// @param _to the new approved address for NFT
    /// @param _tokenId the NFT to be approved
    function approve(
        address _to,
        uint256 _tokenId
    ) public payable virtual override {
        address owner = _ownerAddress[_tokenId];
        require(_to != address(0), "There is no approved address");
        require(owner != _to, "approve owner itelf");
        require(
            msg.sender == owner || isApprovedForAll(owner, msg.sender),
            "not authorized...no rights to approve"
        );
        _approves[_tokenId] = _to;
        emit Approval(owner, _to, _tokenId);
    }

    /// @notice get the approve address for the NFT
    /// @dev throw if _tokenId is invalid
    /// @param _tokenId the NFT to find the approved address
    /// @return the address approved for this NFT
    function getApproved(
        uint256 _tokenId
    ) public view virtual override returns (address) {
        address approvedOwner = _ownerAddress[_tokenId];
        require(
            approvedOwner != address(0),
            "Invalid Address...owner not exist with this tokenId"
        );
        return approvedOwner;
    }

    /// @notice enable and disable approval for the operator to manage msg.sender NFTs
    /// @dev throw if the msg.sender is _operator
    /// @param _operator address to give the authority to operate msg.sender all assets
    /// @param _bo true if it is approved else false
    function setApprovalForAll(
        address _operator,
        bool _bo
    ) public virtual override {
        require(msg.sender != _operator, "Already Approved");
        _approveAllToken[msg.sender][_operator] = _bo;
        emit ApprovalForAll(msg.sender, _operator, _bo);
    }

    /// @notice check the address is approved by owner of NFT
    /// @param _owner the address that own the NFTs
    /// @param _operator the address that acts on behalf of the owner
    /// @return true if _operator is approved for all NFTs by _owner
    function isApprovedForAll(
        address _owner,
        address _operator
    ) public view virtual override returns (bool) {
        return _approveAllToken[_owner][_operator];
    }

    /// @notice transfer ownership of an NFT
    /// @dev throw unless msg.sender is current owner, owner is from address or msg.sender is getapproved by owner
    /// or isapprovedforall by owner, throw if _to is the zero address
    /// @param _from The current owner of the NFT
    /// @param _to The new owner
    /// @param _tokenId The NFT to transfer
    function transferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    ) public payable virtual override {
        address owner = _ownerAddress[_tokenId];
        require(owner == _from, "not token owner");
        require(
            msg.sender == owner ||
                getApproved(_tokenId) == msg.sender ||
                isApprovedForAll(owner, msg.sender),
            "failed"
        );
        require(
            _to != address(0),
            "Invalid Address...reciever address not exist "
        );
        _ownedTokenCount[_to]++;
        _ownedTokenCount[_from]--;
        _ownerAddress[_tokenId] = _to;
        emit Transfer(_from, _to, _tokenId);
    }

    /// @notice transfer the ownership from one address to another address
    /// @dev throw unless msg.sender is current owner, owner is from address or msg.sender is getapproved by owner
    ///  or isapprovedforall by owner, throw if _to is the zero address
    ///  When transfer is complete, this function checks if `_to` is a smart contract (code size > 0). If so, it calls
    ///  onERC721Received on _to and throws if the return value is not `bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"))`
    /// @param _from the current owner of the NFT
    /// @param _to the new owner
    /// @param _tokenId The NFT to transfer
    function safeTransferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    ) public payable virtual override {
        require(_from != _to, "Against Solidity issue #3419");
        transferFrom(_from, _to, _tokenId);

        require(
            _to.code.length == 0 ||
                ERC721TokenReceiver(_to).onERC721Received(
                    msg.sender,
                    _from,
                    _tokenId,
                    ""
                ) ==
                ERC721TokenReceiver.onERC721Received.selector,
            "Failed receipt"
        );
    }

    /// @notice transfer the ownership from one address to another address
    /// @dev throw unless msg.sender is current owner, owner is from address or msg.sender is getapproved by owner
    ///  or isapprovedforall by owner, throw if _to is the zero address
    ///  When transfer is complete, this function checks if `_to` is a smart contract (code size > 0). If so, it calls
    ///  onERC721Received on _to and throws if the return value is not `bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"))`
    /// @param _from the current owner of the NFT
    /// @param _to the new owner
    /// @param _tokenId The NFT to transfer
    /// @param _data additional data
    function safeTransferFrom(
        address _from,
        address _to,
        uint256 _tokenId,
        bytes calldata _data
    ) public payable virtual override {
        transferFrom(_from, _to, _tokenId);

        require(
            _to.code.length == 0 ||
                ERC721TokenReceiver(_to).onERC721Received(
                    msg.sender,
                    _from,
                    _tokenId,
                    _data
                ) ==
                ERC721TokenReceiver.onERC721Received.selector,
            "Failed receipt"
        );
    }
}
